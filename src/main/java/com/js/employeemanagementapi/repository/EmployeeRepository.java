package com.js.employeemanagementapi.repository;

import com.js.employeemanagementapi.entity.EmployeeManagement;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<EmployeeManagement, Long> {
}
