package com.js.employeemanagementapi.controller;

import com.js.employeemanagementapi.model.EmployeeItem;
import com.js.employeemanagementapi.model.EmployeeRequest;
import com.js.employeemanagementapi.model.EmployeeResponse;
import com.js.employeemanagementapi.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping ("/employee")
public class EmployeeController {
    private final EmployeeService employeeService;

    @PostMapping("/people")
    public String setEmployee(@RequestBody EmployeeRequest employeeRequest){
            employeeService.setEmployee(employeeRequest);
            return "입력 완료";
    }

    @GetMapping("/all")
    public List<EmployeeItem> getEmployees() {return employeeService.getEmployees();}

    @GetMapping("/detail/{id}")
    public EmployeeResponse getEmployee(@PathVariable long id){return  employeeService.getEmployee(id);

    }
}
