package com.js.employeemanagementapi.entity;

import com.js.employeemanagementapi.enums.Department;
import com.js.employeemanagementapi.enums.Duty;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@Entity
public class EmployeeManagement {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Integer employeeNumber;

    @Column(nullable = false, length = 20)
    private String name;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private Department affiliatedDepartment;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private Duty dutyClass;

    @Column(nullable = false)
    private Double beforeTax;

    @Column(nullable = false)
    private Double noTax;
}
