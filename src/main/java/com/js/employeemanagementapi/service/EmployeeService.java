package com.js.employeemanagementapi.service;

import com.js.employeemanagementapi.entity.EmployeeManagement;
import com.js.employeemanagementapi.model.EmployeeItem;
import com.js.employeemanagementapi.model.EmployeeRequest;
import com.js.employeemanagementapi.model.EmployeeResponse;
import com.js.employeemanagementapi.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class EmployeeService {

    private final EmployeeRepository employeeRepository;

    public void setEmployee(EmployeeRequest employeeRequest) {
        EmployeeManagement addData = new EmployeeManagement();
        addData.setEmployeeNumber(employeeRequest.getEmployeeNumber());
        addData.setName(employeeRequest.getName());
        addData.setAffiliatedDepartment(employeeRequest.getDepartment());
        addData.setDutyClass(employeeRequest.getDuty());
        addData.setBeforeTax(employeeRequest.getBeforeTax());
        addData.setNoTax(employeeRequest.getNoTax());

        employeeRepository.save(addData);
    }

    public List<EmployeeItem> getEmployees(){
        List<EmployeeManagement> originlist = employeeRepository.findAll();
        List<EmployeeItem> result = new LinkedList<>();

        for (EmployeeManagement employeeManagement : originlist){
            EmployeeItem addItem = new EmployeeItem();
            addItem.setEmployeeNumber(employeeManagement.getEmployeeNumber());
            addItem.setName(employeeManagement.getName());
            addItem.setDepartment(employeeManagement.getAffiliatedDepartment());
            addItem.setDuty(employeeManagement.getDutyClass());


            result.add(addItem);
        }
        return result;
    }
    public EmployeeResponse getEmployee(long id){
        EmployeeManagement originData = employeeRepository.findById(id).orElseThrow();

        double taxApply = 0.299;
        double taxApply2 =0.398;


        EmployeeResponse response = new EmployeeResponse();
        response.setId(originData.getId());
        response.setEmployeeNumber(originData.getEmployeeNumber());
        response.setName(originData.getName());
        response.setDepartment(originData.getAffiliatedDepartment().getDepartment());
        response.setDuty(originData.getDutyClass().getDuty());
        response.setBeforeTax(originData.getBeforeTax());
        response.setNoTax(originData.getNoTax());
        if(originData.getBeforeTax()<46000000) {response.setAfterTax(originData.getBeforeTax() - originData.getBeforeTax() * taxApply);
        } else {response.setAfterTax(originData.getBeforeTax() - originData.getBeforeTax() * taxApply2);
        }
        return response;
    }

}
