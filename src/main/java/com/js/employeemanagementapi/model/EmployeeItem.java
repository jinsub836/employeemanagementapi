package com.js.employeemanagementapi.model;

import com.js.employeemanagementapi.enums.Department;
import com.js.employeemanagementapi.enums.Duty;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmployeeItem {
    private Integer employeeNumber;

    private String name;

    private Department department;

    private Duty duty;

    private String mobile;
}
