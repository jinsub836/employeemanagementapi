package com.js.employeemanagementapi.model;

import com.js.employeemanagementapi.enums.Department;
import com.js.employeemanagementapi.enums.Duty;
import jakarta.persistence.Column;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class EmployeeRequest {
    private Integer employeeNumber;

    private String name;

    @Enumerated(value = EnumType.STRING)
    private Department department;

    @Enumerated(value = EnumType.STRING)
    private Duty duty;

    private Double beforeTax;

    private Double noTax;

}
