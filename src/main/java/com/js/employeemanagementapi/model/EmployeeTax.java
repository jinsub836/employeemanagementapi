package com.js.employeemanagementapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmployeeTax {
    private Double taxApplication;
}
