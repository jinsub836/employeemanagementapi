package com.js.employeemanagementapi.model;

import com.js.employeemanagementapi.enums.Department;
import com.js.employeemanagementapi.enums.Duty;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Setter
@Getter
public class EmployeeResponse {

    private Long id;

    private Integer employeeNumber;

    private String name;

    private String department;

    private String duty;

    private Double beforeTax;

    private Double noTax;

    private Double afterTax;

}
